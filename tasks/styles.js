/**
 * Builds CSS files found in /src/assets/styles
 *
 * @usage gulp styles
 */

import sass from 'gulp-sass';
import browserSync from 'browser-sync';
import cleanCSS from 'gulp-clean-css';
import gulp from 'gulp';
import gulpIf from 'gulp-if';
import sourcemaps from 'gulp-sourcemaps';
import notify from './notify';

let dirSrc = process.env.DIRECTORY_SRC;
let dirDest = process.env.DIRECTORY_DEST;

if (process.env.DIRECTORY_SRC_STYLES != null
    && process.env.DIRECTORY_DEST_STYLES != null
    && process.env.DIRECTORY_SRC_STYLES !== 'false'
    && process.env.DIRECTORY_DEST_STYLES !== 'false'
) {
    dirSrc = process.env.DIRECTORY_SRC_STYLES;
    dirDest = process.env.DIRECTORY_DEST_STYLES;
} else {
    dirSrc = `${dirSrc}/assets/styles`;
    dirDest = `${dirDest}/assets/styles`;
}

function watchStyles() {
    gulp.watch(`${dirSrc}/**/*`, () => {
        notify.log('STYLES: file update detected, rebuilding...');
        buildStyles();
    });
}

function buildStyles() {
    const browser = browserSync.get('local');

    return gulp
        .src(`${dirSrc}/*.scss`)
        .pipe(notify.onError('STYLES: error'))
        .pipe(gulpIf(process.env.SOURCE_MAPS === 'true', sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(gulpIf(process.env.MINIFY === 'true', cleanCSS()))
        .pipe(gulpIf(process.env.SOURCE_MAPS === 'true', sourcemaps.write('./')))
        .pipe(gulp.dest(`${dirDest}/`))
        .on('end', notify.onLog('STYLES: rebuild complete'))
        .on('end', browser.reload);
}

export default function styles() {
    if (process.env.WATCH === 'true') {
        watchStyles();
    }

    return buildStyles();
}
