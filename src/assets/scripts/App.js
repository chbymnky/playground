import $ from 'jquery';
import * as components from './components/index';

/**
 * Application setup
 *
 * @class App
 */
export default class App {
    constructor() {
        // test
        $('.js-welcome').text('Welcome to the client-side boilerplate!');
        this.activeComponents = [];
        this.containerElms = document.querySelectorAll('[data-component]');

        this.activeComponents = this.launchComponents(this.containerElms);
    }

    launchComponents(componentElms) {
        const newCompMods = [];

        componentElms.forEach(elm => {
            const compName = elm.getAttribute('data-component');
            const ComponentMod = components[compName];

            if (ComponentMod != null) {
                newCompMods.push(new ComponentMod(elm));
            }
        });

        return newCompMods;
    }
}
