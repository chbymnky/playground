const MARKUP_CLASSES = {
	open: 'isExpanded',
	closed: 'isCollapsed',
};

/**
 * Finds trigger elements and
 * @param {dom element} element
 */
class Expandables {
	constructor(element) {
		/**
		 * @type {object} dom element
		 * @default undefined
		 */
		this.cntrElem = element;

		/**
		 * @type {string}
		 * @default null
		 */
		this.currState = null;

		/**
		 * @type {string}
		 * @default undefined
		 */
		this.outterTriggerId = this.cntrElem.dataset.triggerId;

		this._setup();
	}

	//
	// SETUP HELPERS
	// --------------------------------------------------------------------------------

	/**
	 * Call to chainable methods that help to setup the properties needed for this
	 * class.
	 */
	_setup() {
		this._setupChildren()
			._setupHandlers();
	}

	_setupChildren() {
		this.drawerElem = this.cntrElem.querySelectorAll('[data-child="drawer"]');

		if (this.outterTriggerId != null) {
			this.externalTriggers = document.querySelectorAll(`[data-trigger*="${this.outterTriggerId}"]`);
		}

		// check for initial state
		if (this.cntrElem.dataset.initState === 'open') {
			this._changeState('open');
		} else if (this.cntrElem.dataset.initState === 'closed') {
			this._changeState('closed');
		} else {
			this._changeState('closed');
		}

		// make chainable
		return this;
	}

	_setupHandlers() {
		this.onToggleExpHandler = this.onToggleExp.bind(this);
		this.onOpenExpHandler = this.onOpenExp.bind(this);
		this.onCloseExpHandler = this.onCloseExp.bind(this);
		this.onClickHandler = this.onClick.bind(this);

		this.cntrElem.addEventListener('click', this.onClickHandler);

		if (this.externalTriggers != null
			&& this.externalTriggers.length > 0
		) {
			this.externalTriggers.forEach((indvTrgr) => {
				indvTrgr.addEventListener('click', this.onClickHandler);
			});
		}

		// make chainable
		return this;
	}

	//
	// PRIVATE HELPERS
	// --------------------------------------------------------------------------------

	_changeState(newState) {
		const newClass = MARKUP_CLASSES[newState];
		let removeClass = newState === 'open' ? 'closed' : 'open';

		removeClass = MARKUP_CLASSES[removeClass];

		this.cntrElem.classList.add(newClass);
		this.cntrElem.classList.remove(removeClass);
		this.currState = newState;
	}

	//
	// EVENT HELPERS
	// --------------------------------------------------------------------------------

	onToggleExp() {
		const newState = this.currState === 'open' ? 'closed' : 'open';

		this._changeState(newState);
	}

	onOpenExp() {
		this._changeState('open');
	}

	onCloseExp() {
		this._changeState('closed');
	}

	onClick(evt) {
		const targetElem = evt.target;
		const currTargetElem = evt.currentTarget;
		let evtTrgrAct = targetElem.dataset.trigger;

		if (currTargetElem.dataset.trigger != null) {
			evtTrgrAct = currTargetElem.dataset.trigger;
		}

		if (evtTrgrAct == null) {
			return false;
		} else if (evtTrgrAct.indexOf(this.outterTriggerId) !== -1) {
			evtTrgrAct = evtTrgrAct.replace(`${this.outterTriggerId}-`, '');
		}

		if (evtTrgrAct === 'open') {
			this.onOpenExpHandler();
		} else if (evtTrgrAct === 'close') {
			this.onCloseExpHandler();
		} else if (evtTrgrAct === 'toggle') {
			this.onToggleExpHandler();
		}
	}
}

export default Expandables;