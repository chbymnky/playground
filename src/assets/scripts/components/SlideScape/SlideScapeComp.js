/**
 * Uses a range input to adjust the position of media content.
 * @class SlideScape
 * @param {DOM Element} element
 */
class SlideScape {
    constructor(element) {
        // expected element props
        const posDir = element.dataset.posDir;
        let slideDir = element.dataset.slideDir;

        if (slideDir == null
            || slideDir !== 'right-to-left'
            || slideDir !== 'left-to-right'
        ) {
            slideDir = 'left-to-right';
        }

        /**
         * @type {DOM ELement}
         * @default element
         */
        this.cntrElem = element;
        /**
         * @type {number}
         * @default 0
         */
        this.currPosition = 0;
        /**
         * @type {string}
         * @default 'left'
         */
        this.posDir = posDir === 'left' || posDir === 'right' ? posDir : 'left';
        /**
         * @type {string}
         * @default 'left-to-right'
         */
        this.slideDir = slideDir;

        this._setup();
    }

    //
    // SETUP METHODS
    // --------------------------------------------------------------------------------

    /**
     * Calls to all necessary setup functions to prepare the component.
     */
    _setup() {
        this._setupChildren()
            ._setupHandlers();
    }

    /**
     * Find all DOM Elements that need for the component and set any values or
     * attributes that might be needed.
     * @chainable
     */
    _setupChildren() {
        this.changeElem = this.cntrElem.querySelector('[data-trigger="change"]');
        this.scapeElems = this.cntrElem.querySelectorAll('[data-child="scape"]');

        if (this.changeElem != null) {
            this._setupRangeVals(this.changeElem);
        }

        // make chainable
        return this;
    }

    /**
     * Create all event handlers needed and bind them to this component.
     * @chainable
     */
    _setupHandlers() {
        this.onChangeHandler = this.onChange.bind(this);

        // bind events to their corresponding elements
        this.changeElem.addEventListener('input', this.onChangeHandler);

        // make chainable
        return this;
    }

    /**
     * calculate and set range values for the input as needed.
     * @chainable
     */
    _setupRangeVals(rangeElem) {
        const setValue = rangeElem.value;

        if (setValue != null && setValue !== 0) {
            rangeElem.value = 0;
        }

        this.rangeMin = 0;
        this.rangeMax = 100;
        this.range = this.rangeMax - this.rangeMin;

        rangeElem.dataset.min = this.rangeMin;
        rangeElem.dataset.max = this.rangeMax;
    }

    //
    // PRIVATE HELPERS
    // --------------------------------------------------------------------------------

    /**
     * Alter the styles of the scapeElems to set styling properties that translate the
     * position of them to the right or left based on the posDir property.
     * @param  {number} newPos
     */
    _repositionScape(newPos) {
        let finalPos = newPos;

        if (this.posDir === 'left') {
            finalPos = finalPos * -1;
        } else if (this.posDir === 'right') {
            finalPos = finalPos * 1;
        }

        this.scapeElems.forEach((indvScape) => {
            indvScape.setAttribute('style', `transform: translate(${finalPos}%, 0)`);
        });
    }

    //
    // EVENT HELPERS
    // --------------------------------------------------------------------------------

    /**
     * Called when the value of the changeElem is altered.
     * @param  {eventObject} evt
     */
    onChange(evt) {
        const currTargetElem = evt.currentTarget;
        const posVal = currTargetElem.value * 1;

        this._repositionScape(posVal);
    }
}

export default SlideScape;
